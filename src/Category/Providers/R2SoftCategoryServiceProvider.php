<?php


namespace R2Soft\Category\Providers;

use R2Soft\Category\Repository\CategoryRepositoryEloquent;
use R2Soft\Category\Repository\CategoryRepositoryInterface;
use Illuminate\Support\ServiceProvider;


class R2SoftCategoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__. '/../../resources/migrations/' => database_path('migrations')
        ], 'migrations');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views/category', 'category');
        require __DIR__ . '/../routes.php';
    }

    public function register()
    {
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepositoryEloquent::class);
    }
}

<?php


namespace R2Soft\Category\Controllers;


use R2Soft\Category\Repository\CategoryRepository;
use R2Soft\Category\Repository\CategoryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class AdminCategoriesController extends  Controller
{
    private $repository;
    /**
     * @var ResponseFactory
     */
    private $response;

    public function __construct(ResponseFactory $response, CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->response = $response;
    }
    public function index()
    {
        $categories = $this->repository->all();
        return $this->response->view('category::index', compact('categories'));
    }

    public function create()
    {
        $categories = $this->repository->all(['id', 'name']);
        $categories = $categories->pluck('name', 'id');
        $nome='r2 soft';
        return view('category::create',  compact('categories', 'nome'));
    }
    public function store(Request $request)
    {
        $this->repository->create($request->all());
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $category = $this->repository->find($id);
        $categories = $this->repository->all();
        return $this->response->view('category::edit', 'category', 'categories');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if(!isset($data['active'])){
            $data['active']=false;
        }else{
            $data['active']=true;
        }

    }
}

<?php
namespace R2Soft\Category\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Validator;

class Category extends Model
{
    use Sluggable;
    protected $table = "code_categories";
    protected $fillable = [
        'name',
        'slug',
        'active',
        'parent_id',
    ];
    private $validador;

    public function setValidator(Validator $validator)
    {
        $this->validador = $validator;
    }

    public function getValidator()
    {
        return $this->validador;
    }
    public function isValid(){
        $validator = $this->validador;
        $validator->setRules(['name'=>'required|max:255']);
        $validator->setData($this->getAttributes());
        if ($validator->fails()){
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }
    public function categorizable()
    {
        return $this->morphTo();
    }
    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class,'parent_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'             => 'name',
                'maxLength'          => null,
                'maxLengthKeepWords' => true,
                'method'             => null,
                'separator'          => '-',
                'unique'             => true,
                'uniqueSuffix'       => null,
                'includeTrashed'     => false,
                'reserved'           => null,
                'onUpdate'           => false,
            ]
        ];
    }
}

<?php


namespace R2Soft\Category\Repository;

use R2Soft\Database\Contracts\CriteriaCollection;
use R2Soft\Database\Contracts\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface, CriteriaCollection
{


}

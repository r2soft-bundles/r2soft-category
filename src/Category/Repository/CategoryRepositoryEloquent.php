<?php


namespace R2Soft\Category\Repository;

use R2Soft\Category\Models\Category;
use R2Soft\Database\AbstractRepository;
use R2Soft\Database\Contracts\CriteriaCollection;

class CategoryRepositoryEloquent extends AbstractRepository implements CategoryRepositoryInterface
{

    public function model()
    {
        return Category::class;
    }

}

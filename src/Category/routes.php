<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix'=>'admin/categories', 'as'=>'categories.','namespace'=>'R2Soft\Category\Controllers', 'middleware'=>['web']], function (){
   Route::get('', ['uses'=>'AdminCategoriesController@index', 'as'=>'index']) ;
   Route::get('/create', ['uses'=>'AdminCategoriesController@create', 'as'=>'create']) ;
   Route::post('/store', ['uses'=>'AdminCategoriesController@store', 'as'=>'store']) ;
});

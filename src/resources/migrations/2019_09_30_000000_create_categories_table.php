<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class CreateCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('code_categories', function (Blueprint $table){
            $table->increments('id');
            $table->integer('parent_id')->nullable(true)->unsigned();
            $table->foreign('parent_id')->references('id')->on('code_categories');
            $table->string('name');
            $table->string('slug');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('code_categories');
    }
}


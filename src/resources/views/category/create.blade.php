@extends('layouts.app')

@section('content')
    <div class="container">

        <h3>Create Category 2</h3>

        {!! Form::open(['method'=>'post', 'route'=>'categories.store']) !!}
        <div class="form-group">
            {!! Form::label('Parent', 'Parent:') !!}
            {!! Form::select('parent_id',$categories, null, ['placeholder' => '-- Parent Category --', 'class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Name', 'Name:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Active', 'Active:') !!}
            {!! Form::checkbox('active', true, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Create Category', ['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection

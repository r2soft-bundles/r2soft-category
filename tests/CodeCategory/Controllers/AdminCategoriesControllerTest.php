<?php


namespace R2Soft\Category\Tests\Controllers;


use AbstractTestCase;
use R2Soft\Category\Controllers\AdminCategoriesController;
use R2Soft\Category\Controllers\Controller;
use R2Soft\Category\Models\Category;
use R2Soft\Category\Repository\CategoryRepository;
use R2Soft\Category\Repository\CategoryRepositoryEloquent;
use Illuminate\Routing\ResponseFactory;
use Mockery as m;

class AdminCategoriesControllerTest extends AbstractTestCase
{
    public function test_should_extend_from_controller()
    {
        $repository = m::mock(CategoryRepositoryEloquent::class);
        $responseFactory = m::mock(ResponseFactory::class);
        $controller = new AdminCategoriesController($responseFactory, $repository);

        $this->assertInstanceOf(Controller::class, $controller);
    }

    public function test_controller_should_run_index_and_return_correct_arguments()
    {
        $repository = m::mock(CategoryRepositoryEloquent::class);
        $responseFactory = m::mock(ResponseFactory::class);
        $controller = new AdminCategoriesController($responseFactory, $repository);
        $html = m::mock();
        $categoriesResult = ['Cat1', 'Cat2'];
        $repository->shouldReceive('all')->andReturn($categoriesResult);
        $responseFactory->shouldReceive('view')
            ->with('category::index', ['categories'=>$categoriesResult])
            ->andReturn($html);
        $this->assertEquals($controller->index(), $html);
    }
}
